A quick and dirty example showing how to transfer custom Java objects between Swing and JavaFX applications.

This project uses Gradle. It is split up into the top level project along with three subprojects. The three subprojects are:

1. `javafx`
2. `model`
3. `swing`

The `javafx` project holds the JavaFX application code, the `swing` proejct holds the Swing application code, and the `model` project holds a single class that is used for transfering information between the two applications. The top level project has the main entry point which launches the Swing and JavaFX apps in separate processes and waits for them to exit before exiting itself. It is set up so that if either the
JavaFX app or the Swing app exits the other process will be destroyed as well.

To run this example simply execute `.\gradlew run` in the command line.

*Note: This example requires Java 10 as I used APIs added in that version. These APIs are mostly for utility, however, and the actual Swing and
JavaFX code should be the same as if written for Java 8.*