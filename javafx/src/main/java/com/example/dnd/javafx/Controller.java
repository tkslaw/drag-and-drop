package com.example.dnd.javafx;

import com.example.dnd.model.Doctor;
import java.util.ArrayList;
import java.util.List;
import javafx.fxml.FXML;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DataFormat;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;

public class Controller {

  /*
   * The MIME types passed to JavaFX from Swing seem to be prepended by "JAVA_DATAFLAVOR:". I
   * don't know if that's a stable implementation detail but I could not readily find documentation
   * about it. I discovered it through trial-and-error.
   *
   * The second MIME type here is simply so the DataFormat has a MIME type that is equal to the
   * first in every way except for "JAVA_DATAFLAVOR:". Don't know if that's necessary.
   *
   * The class parameter is necessary for the Swing application to be able to deserialize the
   * data, I think.
   */
  private static final DataFormat DOCTOR_FORMAT = new DataFormat(
      "JAVA_DATAFLAVOR:application/x-my-mime-type; class=java.util.ArrayList",
      "application/x-my-mime-type; class=java.util.ArrayList"
  );

  @FXML private TableView<Doctor> table;

  @FXML
  private void initialize() {
    table.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

    /*
     * Tried to set this via FXML but it wouldn't see the method when the parameter
     * of #dragDetected was a MouseEvent. It *would* see it when I changed the parameter type
     * to a DragEvent but that quite predictably threw an exception when called... maybe I did
     * something wrong but it could also be a bug (Java 10.0.2).
     */
    table.setOnDragDetected(this::dragDetected);
  }

  private void dragDetected(MouseEvent event) {
    if (!table.getSelectionModel().isEmpty()) {
      Dragboard db;
      /*
       * Reading the Swing tutorials on "Drag and Drop" mentioned it's common behavior that having
       * control down during a DnD means to only allow copying the data rather than also allowing
       * moving the data. At the very least, that's how Swing components tend to handle it.
       */
      if (event.isControlDown()) {
        db = table.startDragAndDrop(TransferMode.COPY);
      } else {
        db = table.startDragAndDrop(TransferMode.COPY_OR_MOVE);
      }

      ArrayList<Doctor> data = new ArrayList<>(table.getSelectionModel().getSelectedItems());
      ClipboardContent content = new ClipboardContent();
      content.put(DOCTOR_FORMAT, data);
      db.setContent(content);

      event.consume();
    }
  }

  @FXML
  private void dragOver(DragEvent event) {
    // Technically you'd also want to check if the TableView is disabled or not. Maybe also the
    // editable state too.
    if (event.getGestureSource() != table && event.getDragboard().hasContent(DOCTOR_FORMAT)) {
      event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
      event.consume();
    }
  }

  @FXML
  private void dragDropped(DragEvent event) {
    if (event.isAccepted()) {
      table.getItems().addAll((ArrayList<Doctor>) event.getDragboard().getContent(DOCTOR_FORMAT));
      event.setDropCompleted(true);
      event.consume();
    }
  }

  @FXML
  private void dragDone(DragEvent event) {
    if (event.getTransferMode() == TransferMode.MOVE) {
      // If the accepted action was MOVE then we must remove all the transferred data from
      // this TableView
      table.getItems().removeAll(List.copyOf(table.getSelectionModel().getSelectedItems()));
    }
  }

}
