package com.example.dnd;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public class Main {

  private static final String JAVAFX_CLASS = "com.example.dnd.javafx.App";
  private static final String SWING_CLASS = "com.example.dnd.swing.MainFrame";

  public static void main(String[] args) throws IOException {
    System.out.println("Main PID = " + ProcessHandle.current().pid());

    String javaExe = Paths.get(System.getProperty("java.home"))
                          .resolve("bin").resolve("java").toString();
    String classpath = System.getProperty("java.class.path");

    Process javafxProc = new ProcessBuilder(List.of(javaExe, "-cp", classpath, JAVAFX_CLASS))
        .inheritIO()
        .start();
    Process swingProc = new ProcessBuilder(List.of(javaExe, "-cp", classpath, SWING_CLASS))
        .inheritIO()
        .start();

    CompletableFuture.allOf(
        javafxProc.onExit().thenRun(swingProc::destroy),
        swingProc.onExit().thenRun(javafxProc::destroy)
    ).join();

  }

}
