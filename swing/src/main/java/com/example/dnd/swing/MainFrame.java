package com.example.dnd.swing;

import com.example.dnd.model.Doctor;
import java.awt.Dimension;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DropMode;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;

public class MainFrame extends JFrame {

  public static void main(String[] args) {
    System.out.println("Swing Application PID = " + ProcessHandle.current().pid());
    SwingUtilities.invokeLater(() -> new MainFrame().setVisible(true));
  }

  private MainFrame() {
    JPanel panel = new JPanel();
    panel.setPreferredSize(new Dimension(600, 400));
    panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

    JLabel label = new JLabel("Doctors");
    label.setAlignmentX(JLabel.CENTER_ALIGNMENT);
    label.setBorder(BorderFactory.createEmptyBorder(15, 0, 5, 0));
    panel.add(label);

    JScrollPane scroll = new JScrollPane(createJTable());
    scroll.setBorder(BorderFactory.createEmptyBorder(0, 15, 15, 15));
    panel.add(scroll);

    add(panel);
    pack();
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    setLocationRelativeTo(null);
    setTitle("Drag and Drop - Swing Application");
  }

  private JTable createJTable() {
    JTable table = new JTable();

    DoctorTableModel model = new DoctorTableModel();
    table.setModel(model);
    model.addAll(List.of(
        new Doctor("William", "Hartnell", 1),
        new Doctor("Patrick", "Troughton", 2),
        new Doctor("Jon", "Pertwee", 3),
        new Doctor("Tom", "Baker", 4),
        new Doctor("Peter", "Davison", 5),
        new Doctor("Colin", "Baker", 6),
        new Doctor("Sylvester", "McCoy", 7)
    ));

    table.setDragEnabled(true);
    table.setDropMode(DropMode.INSERT_ROWS);
    table.setTransferHandler(new DoctorTransferHandler());
    table.setFillsViewportHeight(true);

    return table;
  }

}
