package com.example.dnd.swing;

import com.example.dnd.model.Doctor;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import javax.swing.table.AbstractTableModel;

public class DoctorTableModel extends AbstractTableModel {

  private static final List<String> COL_NAMES = List.of("First Name", "Last Name", "Number");
  private static final List<Class<?>> COL_TYPES = List.of(String.class, String.class, int.class);
  private static final List<Function<Doctor, Object>> VAL_FUNCS =
      List.of(Doctor::getFirstName, Doctor::getLastName, Doctor::getNumber);

  private final List<Doctor> data = new ArrayList<>();

  public Doctor get(int index) {
    return data.get(index);
  }

  public List<Doctor> getAll(int... indices) {
    return IntStream.of(indices).mapToObj(data::get).collect(Collectors.toList());
  }

  // These modification methods all end up calling #fireTableDataChanged() which
  // I'm sure is not the most efficient way of doing things but it is the easiest.

  public void add(Doctor doctor) {
    add(data.size(), doctor);
  }

  public void add(int index, Doctor doctor) {
    data.add(index, doctor);
    fireTableDataChanged();
  }

  public void addAll(Collection<? extends Doctor> doctors) {
    data.addAll(doctors);
    fireTableDataChanged();
  }

  public void remove(int index) {
    data.remove(index);
    fireTableDataChanged();
  }

  public void remove(Doctor doctor) {
    remove(data.indexOf(doctor));
  }

  public void removeAll(Collection<? extends Doctor> doctors) {
    doctors.forEach(d -> remove(data.indexOf(d)));
  }

  @Override
  public String getColumnName(int column) {
    return COL_NAMES.get(column);
  }

  @Override
  public Class<?> getColumnClass(int columnIndex) {
    return COL_TYPES.get(columnIndex);
  }

  @Override
  public int getRowCount() {
    return data.size();
  }

  @Override
  public int getColumnCount() {
    return COL_NAMES.size();
  }

  @Override
  public Object getValueAt(int rowIndex, int columnIndex) {
    return VAL_FUNCS.get(columnIndex).apply(data.get(rowIndex));
  }

}
